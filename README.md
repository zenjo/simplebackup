Script de backup avec rsync, pour répertoires locaux ou réseau et sites distants via ssh. Le script, en jouant sur une option de rsync (hard links) fait des backups incrémentaux de 1 à 100 prévu; le rythme sera celui de l'exécution du script, via cron par exemple.

Les backups successifs seront dans l’ordre dans backupAAAAMMJJ_HHMMSS.0, backupAAAAMMJJ_HHMMSS.1, .2, ... .n.

- Usage basique: ./simplebackup nb_increments source cible [chemin/absolu/cle_privee]
- A partir de la 1.2, introduction de scripts de backup propre à SimpleBackup. `Usage: ./simplebackup -f <config_file>`
Tous les &lt;config_file> devant se trouver dans le sous répertoire ./scripts/
- A partir de la 1.3, possibilité d'attacher un fichier d'exclusion à un script SimpleBackup
Tous les fichiers d'exclusion devant se trouver dans le sous répertoire ./scripts/ et leur nom doit se terminer par -exclude
- A partir de la 2.0, gestion des restaurations via un explorateur de backups. Restauration de répertoires, pas de fichiers individuels. 

>[Aide complète](https://framagit.org/zenjo/simplebackup/wikis/Aide-2.0).

Langues: actuellement FR, EN 
